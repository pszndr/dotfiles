#!/bin/bash

player_status=$(playerctl status 2> /dev/null)

if [[ $? -eq 0 ]]; then
    if [[ $(playerctl metadata artist) = "" ]]; then
        metadata="$(playerctl metadata title)"
    else
        metadata="$(playerctl metadata title), by $(playerctl metadata artist)"
    fi
fi

if [[ $player_status = "Playing" ]]; then
    echo "%{F#ffd0c3e1}  $metadata" # when playing
elif [[ $player_status = "Paused" ]]; then
    echo "%{F#77d0c3e1}  $metadata" # when paused
else
    echo "%{F#77d0c3e1}" # when stopped
fi
